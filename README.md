# custom-fields

a [Saleshandy Custom fields](https://documenter.getpostman.com/view/3193762/TVCh1TF1) application


### Links

+ [API documentation](https://documenter.getpostman.com/view/3193762/TVCh1TF1)

## How to run project
1. Create database use `data/database.sql` file
2. Change database connection in `config/datastores.js` if required
3. This project is build on sails js, [install sails js globally](https://sailsjs.com/get-started)
4. run `npm install` to install all dependency in project root folder
5. once all the dependencies are installed, run `sails lift`

### Need help? Contact Author
+ [Rutvik Prajapati](mailto:rut2prajapati@gmail.com)
