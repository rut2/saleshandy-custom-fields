module.exports = {


  friendlyName: 'Create new customer',


  description: 'Create new customer with custom fields.',


  inputs: {
    firstName: {
      type: 'string',
      required: true
    },

    lastName: {
      type: 'string',
      required: true
    },

    emailAddress: {
      type: 'string',
      isEmail: true,
      required: true
    },

    properties: {
      type: 'ref'
    }

  },


  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    },

  },


  fn: async function (inputs) {

    let newCustomerRecord = await Customers.create({
      firstName: inputs.firstName,
      lastName: inputs.lastName,
      emailAddress: inputs.emailAddress
    })
      .intercept('E_UNIQUE', 'emailAlreadyInUse')
      .intercept({name: 'UsageError'}, 'invalid')
      .fetch();

    let properties = _.map(inputs.properties, (property)  => {
      return _.extend({}, property, {customer: newCustomerRecord.id});
    });


    await PropertiesValue.createEach(properties).intercept({name: 'UsageError'}, 'invalid');

    newCustomerRecord = await Customers.findOne({id: newCustomerRecord.id});

    const SQL_QUERY = `select p.id as id, p.name, v.value
                      from properties p
                      inner join properties_value v on v.propertyId = p.id
                      where v.customerId = $1`;

    let propertyRecords = await sails.sendNativeQuery(SQL_QUERY, [newCustomerRecord.id]);



    await this.res.json({...newCustomerRecord, properties: propertyRecords.rows});

  }


};
