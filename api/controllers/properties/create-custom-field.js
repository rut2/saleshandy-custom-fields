module.exports = {


  friendlyName: 'Create new field',


  description: 'Create new custom field.',


  inputs: {

    name: {
      type: 'string',
      required: true
    },

    component: {
      type: 'number',
      required: true
    },

    description: {
      type: 'string',
      required: true
    }
  },


  exits: {

  },


  fn: async function (inputs) {

    const newFieldRecord = await Properties.create({
      ...inputs
    }).fetch();

    const newFieldRecordWithComponent = await Properties.findOne({id: newFieldRecord.id}).populate('component');

    await this.res.json(newFieldRecordWithComponent);

  }


};
